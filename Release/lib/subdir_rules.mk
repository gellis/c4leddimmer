################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
lib/button_device.obj: ../lib/button_device.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/button_device.d" --obj_directory="lib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/common.obj: ../lib/common.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/common.d" --obj_directory="lib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/led_device.obj: ../lib/led_device.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/led_device.d" --obj_directory="lib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/pwm_device.obj: ../lib/pwm_device.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/pwm_device.d" --obj_directory="lib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/rgb_led.obj: ../lib/rgb_led.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/rgb_led.d" --obj_directory="lib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


