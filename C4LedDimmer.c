/**
 @file
 @date    6/6/2016
 @author  Glenn Ellis


  Pinout Table
  ------------

  GPIO Pin | Device
  ---------|---------
  PF0      | USR_SW2
  PF1      | LED_R
  PF2      | LED_G
  PF3      | LED_B
  PF4      | USR_SW1
 */

#include <stdbool.h>
#include <stdint.h>

#include "common.h"
#include "rgb_led.h"
#include "button_device.h"
#include "iomap.h"

// Function declerations
void ButtonHandler(void);

/**
  State machine states
*/
typedef enum DimmerStatesEnum
{
  DoNothing,
  IncreaseBrightness,
  DecreaseBrightness,
  RampUp,
  RampDown,
  CheckButtons,
  MAX_DIMMER_STATES
} DimmerStatesEnum;

/// Global State variable
DimmerStatesEnum dimmerState;
ButtonDevice btn;
enum
{
  DOWN,
  UP
};
enum
{
  NONE,
  RAMP,
  ONCE
};

static const uint8_t DELAY = 20;

int main(void)
{

  uint8_t brightness = 50;
  RGBLed led; // instansiate RGBLed

  IOSetup();

  // Initialize button structure
  btn.func = ButtonHandler;
  btn.button[UP].gpioPin = BUTTON_UP;
  btn.button[DOWN].gpioPin = BUTTON_DOWN;

  ButtonInit(&btn);

  RGB_Init(&led);
  RGB_SetBrightness(&led, brightness);

  while (true)
  {
    switch (dimmerState)
    {
    case IncreaseBrightness:
      brightness = InRange(brightness + 1, 0, 100);
      RGB_SetBrightness(&led, brightness);
      dimmerState = DoNothing;
      break;
    case DecreaseBrightness:
      brightness = InRange(brightness - 1, 0, 100);
      RGB_SetBrightness(&led, brightness);
      dimmerState = DoNothing;
      break;
    case RampUp:
      RBG_RampUp(&led, DELAY);
      brightness = 100;
      dimmerState = DoNothing;
      break;
    case RampDown:
      RBG_RampDown(&led, DELAY);
      brightness = 0;
      dimmerState = DoNothing;
      break;
    case DoNothing:
    default:
      break;
    };

    delay_ms(DELAY);
  }
}

static int ProcessButton(ButtonStruct *btn)
{
  uint32_t holdTime = GetButtonHoldTime(btn);

  int ret = NONE;

  if (holdTime < 25)
    return ret;

  if (holdTime > 1000 && IsButtonPressed(btn))
  {
    ret = ONCE;
  }
  else if (holdTime < 1000 && !IsButtonPressed(btn))
  {
    ResetButtonTimer(btn);
    ret = RAMP;
  }
  else if (!IsButtonPressed(btn))
  {
    ResetButtonTimer(btn);
  }
  return ret;
}

/**
  Button Interrupt handler

*/
void ButtonHandler(void)
{

  ButtonStruct *upButton = &(btn.button[UP]);
  ButtonStruct *downButton = &(btn.button[DOWN]);

  int upResult = NONE;
  int downResult = NONE;

  upResult = ProcessButton(upButton);
  switch (upResult)
  {
  case RAMP:
    dimmerState = RampUp;
    break;
  case ONCE:
    dimmerState = IncreaseBrightness;
    break;
  default:
    downResult = ProcessButton(downButton);
    break;
  };

  switch (downResult)
  {
  case RAMP:
    dimmerState = RampDown;
    break;
  case ONCE:
    dimmerState = DecreaseBrightness;
    break;
  default:
    break;
  };
}
