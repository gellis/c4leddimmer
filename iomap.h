/**
  @file
  @date   6/8/2016 2:33:24 PM
  @author Glenn Ellis
*/

#ifndef _IO_MAP_H
#define _IO_MAP_H

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

// Button Mapping
#define BUTTON_NUM         	2
#define BUTTON_UP           GPIO_PIN_4
#define BUTTON_DOWN        	GPIO_PIN_0

#define BUTTON_BASE         GPIO_PORTF_BASE
#define BUTTON_PERIPH       SYSCTL_PERIPH_GPIOF
#define ALL_BUTTONS         (BUTTON_UP | BUTTON_DOWN)

#define BUTTON_PERIPH_TIMER  SYSCTL_PERIPH_TIMER5
#define BUTTON_TIMER_BASE    TIMER5_BASE
#define BUTTON_TIMER         TIMER_A
#define BUTTON_TIMER_TIMA    TIMER_TIMA_TIMEOUT
// These settings are based on a 16Mhz system clock
#define BUTTON_TIMMER_PERIOD  8000 //1ms

void IOSetup();

#endif // _IO_MAP_H
