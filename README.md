LED Dimmer Application for Control4 Interview
==============================================
_________________________________________________________________________________________

Author of Code: Glenn Ellis

Challenge Summary
------------------
Create a simple image used to control the intensity of the LED located on the provided Tiva TMC123G Launch Pad.  There is no time limit for this challenge, however, it should take you about 3 hours to complete. If you have any questions, please contact your recruiter or Control4 representative.

Specification
---------------
- The LED will be capable of generating a continuous intensity of white (or as close as possible to white…) light over a 0-100 scale of intensity.
- Press and hold of SW1 will ramp the LED brightness up to maximum brightness level (100).
- Press and hold of SW2 will ramp the LED brightness down to the minimum brightness level (0 or LED off).
- The rate of the ramp up or down will take a maximum of four seconds.
- If the button is released, the ramp will stop and the current intensity of the LED will be maintained.
- You may add other enhancements in addition to these capabilities if you should choose to do so, however, everything you implement should be polished and glitch free.  (Quality feature implementation beats feature quantity…)

Bonus
------
- Press and release of SW1 for less than one second will cause the LED to ramp to maximum brightness level (100) within four seconds.
- Press and release of SW2 for less than one second will cause the LED to ramp to minimum brightness value (0 or LED off) within four seconds.
 
Compiler and Documentation
----------------------------
The link for the compiler and all relevant documentation can be found in the Quick Start card included with the Tiva TMC123G Launch Pad.
 
Deliverable
------------
Provide all relevant projects, source code, and documentation to allow for a proper execution and demonstration of your image to your recruiter or Control4 representative.  You may keep the Tiva TMC123G Launch Pad.
Remember that all code must be your own.  If you choose to use code written by others, please make a note of the source it in the documentation.

Reference Material
===================
_________________________________________________________________________________________

Online Documentation
---------------------
- [Tiva C Series TM4C123G product page](http://www.ti.com/tool/ek-tm4c123gxl#descriptionArea)
- [TM4C123GH6PM Data Sheet](http://www.ti.com/lit/ds/spms376e/spms376e.pdf)
- [TivaWare™ Peripheral Driver Library for C Series User's Guide](http://www.ti.com/lit/pdf/spmu298)
- [Tiva™ C Series TM4C123x ROM User’s Guide](http://www.ti.com/lit/pdf/spmu367)
- [Tiva C Series TM4C123G Getting Started](http://processors.wiki.ti.com/index.php/Getting_Started_with_the_TIVA%E2%84%A2_C_Series_TM4C123G_LaunchPad)
- [Tiva™ C Series TM4C123x Workshop Workbook](http://software-dl.ti.com/trainingTTO/trainingTTO_public_sw/GSW-TM4C123G-LaunchPad/TM4C123G_LaunchPad_Workshop_Workbook.pdf)


Pinout for TM4C123GXL Eval kit
--------------------------------
 
GPIO Pin | Device
---------|---------
PF0      | USR_SW2
PF1      | LED_R
PF2      | LED_G
PF3      | LED_B
PF4      | USR_SW1

File List
===========
_________________________________________________________________________________________

File                                        | Description
--------------------------------------------|---------------------------------------------
[C4LedDimmer.c](C4LedDimmer.c)              | Main Program File
[iomap.h](iomap.h)                          | Defines all the IO required for the program
[iomap.c](iomap.c)                          | Implementation of initialization of IO
[button_device.h](lib/button_device.h)      | Button Driver declaration
[button_device.c](lib/button_device.c)      | Button Driver implementation
[common.h](lib/common.h)                    | Declaration of a common set of functions
[common.c](lib/common.c)                    | Implementation of a common set of functions
[led_device.h](lib/led_device.h)            | Defines an abstract LED device driver
[led_device.c](lib/led_device.c)            | Implementation of an abstract LED device driver
[pwm_device.h](lib/pwm_device.h)            | Defines an abstract pwm device driver
[pwm_device.c](lib/pwm_device.c)            | Implementation an abstract pwm device driver
[rgb_led.h](lib/rgb_led.h)                  | Defines a device driver for an RGB LED
[rgb_led.c](lib/rgb_led.c)                  | Implements a device driver for an RGB LED

Diagrams
==========
_________________________________________________________________________________________

![System Diagram](doc/images/C4LedDimmer.png)