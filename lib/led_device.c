/**
    @file
    @date 	6/6/2016
    @author	Glenn Ellis

*/

#include "common.h"
#include "led_device.h"

void LED_Init(LEDDevice *led)
{
    led->pwm.freq = 100000; ///< Set pwm freqency to 100kHz
    PWM_Init(&led->pwm);
}

void LED_SetBrightness(LEDDevice *led, uint8_t value)
{
    uint8_t newValue = InRange(value, 0, 100);
    if (newValue == 0)
    {
        LED_TurnOff(led);
        return;
    }
    else
    {
        LED_TurnOn(led);
    }
    PWM_SetDutyCycle(&led->pwm, newValue);
}

void LED_TurnOn(LEDDevice *led)
{
    PWM_Enable(&led->pwm);
}

void LED_TurnOff(LEDDevice *led)
{
    PWM_Disable(&led->pwm);
}
