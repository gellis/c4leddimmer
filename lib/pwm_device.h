/**
  @file
  @date   6/6/2016
  @author   Glenn Ellis

   References
   ==========
   [Tiva C Series TM4C123x ROM User's Guide](http://www.ti.com/lit/ug/spmu367/spmu367.pdf)
   [Tiva C Series TM4C123x Peripheral Driver Library](http://www.ti.com/lit/ug/spmu298a/spmu298a.pdf)
 */

#ifndef _PWM_DEVICE_H
#define _PWM_DEVICE_H

#include <stdint.h>
#include <stdbool.h>
#include "driverlib/pwm.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"

#define PWM_DEFAULT_FREQ 100000 ///< 100kHz #define PWM_DEFAULT_DUTY  50
#define PWM_DEFAULT_DUTY 50

typedef struct PWMDevice
{
  uint32_t pwmPinCfg; ///< Pin Configuration type address (i.e. GPIO_PF1_M1PWM5)
  uint32_t base;      ///< Pulse Width Modulator (PWM) Base (i.e. PWM1_BASE)
  uint32_t out;       ///< Encoded offset address of the PWM (i.e. PWM_OUT_5)
  uint32_t bit;       ///< Bit-wise ID for PWM (i.e. PWM_OUT_5_BIT)
  uint32_t gen;       ///< Offset address of PWM Gen (i.e. PWM_GEN_0)
  uint32_t periph;    ///< Setting for which PWM Periphial to use (i.e. SYSCTL_PERIPH_PWM1)
  uint32_t modes;     ///< PWM Configuration Modes (i.e. PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC)
  uint32_t freq;      ///< PWM frequency
  uint32_t gpioBase;
  uint32_t gpioPin;

  uint32_t period;
  uint32_t pulseWidth;
} PWMDevice;

void PWM_Init(PWMDevice *pwm);
void PWM_SetDutyCycle(PWMDevice *pwm, uint8_t value);
uint8_t PWM_GetDutyCycle(PWMDevice *pwm);
void PWM_Enable(PWMDevice *pwm);
void PWM_Disable(PWMDevice *pwm);
void PWM_SetPeriod(PWMDevice *pwm, uint32_t period);

#endif // _PWM_DEVICE_H
