/**
  @file
  @date   6/6/2016
  @author   Glenn Ellis

  References
  ==========


 */

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/pin_map.h"

#include "pwm_device.h"

void PWM_Init(PWMDevice *pwm)
{

  // Set period and Duty Cycle
  uint32_t sysClk = SysCtlClockGet();
  pwm->period = (sysClk / pwm->freq);
  PWM_SetPeriod(pwm, pwm->period);
  PWM_SetDutyCycle(pwm, PWM_DEFAULT_DUTY);

  // Enable Generator
  MAP_PWMGenEnable(pwm->base, pwm->gen);
}

void PWM_SetPeriod(PWMDevice *pwm, uint32_t period)
{
  pwm->period = period;
  PWMGenPeriodSet(pwm->base, pwm->gen, period);
}

uint32_t PWM_GetPeriod(PWMDevice *pwm)
{
  return pwm->period;
}

uint8_t PWM_GetDutyCycle(PWMDevice *pwm)
{
  // dutyCycle = Puls width * Period / 100
  uint32_t period = PWM_GetPeriod(pwm);
  return (pwm->pulseWidth / period * 100);
}

void PWM_SetDutyCycle(PWMDevice *pwm, uint8_t dutyCycle)
{
  pwm->pulseWidth = ((dutyCycle * pwm->period) / 100);
  PWMPulseWidthSet(pwm->base, pwm->out, pwm->pulseWidth);
}

void PWM_Enable(PWMDevice *pwm)
{
  PWMOutputState(pwm->base, pwm->bit, true);
}

void PWM_Disable(PWMDevice *pwm)
{
  PWMOutputState(pwm->base, pwm->bit, false);
}
