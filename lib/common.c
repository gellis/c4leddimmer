/*
 * @file
 *
 * @date    Jun 5, 2016
 * @author  Glenn Ellis
 */

#include <stdint.h>
#include <stdbool.h>
#include "driverlib/sysctl.h"

#include "common.h"

void delay_ms(int ms)
{
    SysCtlDelay((SysCtlClockGet() / (3 * 1000)) * ms);
}

int InRange(int value, int min, int max)
{
    if (value >= max)
        return max;
    if (value <= min)
        return min;
    return value;
}
