/**
  @file

  @date Jun 6, 2016
  @author Glenn Ellis

*/
#include <stdint.h>
#include <stdbool.h>
#include "driverlib/timer.h"
#include "button_device.h"

ButtonDevice *g_ButtonDevice;
uint8_t milliseconds;

static void ButtonRead();
static void buttonISR(void);

void ButtonInit(ButtonDevice *btn)
{
  int i;
  for (i = 0; i < BUTTON_NUM; i++)
  {
    btn->button[i].holdTime = 0;
    btn->button[i].pressed = false;
  }
  g_ButtonDevice = btn;

  //-----------------------------------------------------------------------
  // Setup Button Port and Pins
  //-----------------------------------------------------------------------

  // Enable the GPIO port to which the pushbuttons are connected.
  MAP_SysCtlPeripheralEnable(BUTTON_PERIPH);

  // Unlock PF0 so we can change it to a GPIO input
  // Once we have enabled (unlocked) the commit register then re-lock it
  // to prevent further changes.  PF0 is muxed with NMI thus a special case.
  HWREG(BUTTON_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
  HWREG(BUTTON_BASE + GPIO_O_CR) |= 0x01;
  HWREG(BUTTON_BASE + GPIO_O_LOCK) = 0;

  // Set each of the button GPIO pins as an input with a pull-up.
  MAP_GPIODirModeSet(BUTTON_BASE, ALL_BUTTONS, GPIO_DIR_MODE_IN);
  MAP_GPIOPadConfigSet(BUTTON_BASE, ALL_BUTTONS,
                       GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

  //-----------------------------------------------------------------------
  // Setup Timer and Timer Interrupt
  //-----------------------------------------------------------------------

  // Enable Time Peripheral
  SysCtlPeripheralEnable(BUTTON_PERIPH_TIMER);
  SysCtlDelay(3);

  TimerConfigure(BUTTON_TIMER_BASE, TIMER_CFG_PERIODIC);
  TimerLoadSet(BUTTON_TIMER_BASE, BUTTON_TIMER, BUTTON_TIMMER_PERIOD - 1);

  TimerIntRegister(BUTTON_TIMER_BASE, BUTTON_TIMER, buttonISR);

  TimerIntEnable(BUTTON_TIMER_BASE, BUTTON_TIMER_TIMA);
  TimerEnable(BUTTON_TIMER_BASE, BUTTON_TIMER);
}

uint32_t GetButtonHoldTime(ButtonStruct *btn)
{
  return btn->holdTime;
}

bool IsButtonPressed(ButtonStruct *btn)
{
  return btn->pressed;
}

void ResetButtonTimer(ButtonStruct *btn)
{
  btn->holdTime = 0;
}

static void ButtonRead()
{
  uint8_t value = (uint8_t)GPIOPinRead(BUTTON_BASE, ALL_BUTTONS);

  int i;
  for (i = 0; i < BUTTON_NUM; i++)
  {
    ButtonStruct *btn = &(g_ButtonDevice->button[i]);

    if ((btn->pressed = !(value & btn->gpioPin)))
    {
      btn->holdTime++;
    }
  }
}

static void buttonISR(void)
{

  uint32_t status = 0;
  status = TimerIntStatus(TIMER5_BASE, true);
  TimerIntClear(TIMER5_BASE, status);

  ButtonRead();

  // Run Button Callback function to check for button pressed
  g_ButtonDevice->func();
}
