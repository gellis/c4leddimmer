/*
 * common.h
 *
 *  Created on: Jun 5, 2016
 *      Author: Glenn Ellis
 */

#ifndef LIB_COMMON_H_
#define LIB_COMMON_H_

#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "inc/hw_types.h"

void delay_ms(int ms);
int InRange(int value, int min, int max);
#endif /* LIB_COMMON_H_ */
