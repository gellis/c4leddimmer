################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/adc.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/aes.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/can.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/comp.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/cpu.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/crc.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/des.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/eeprom.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/emac.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/epi.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/flash.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/fpu.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/gpio.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/hibernate.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/i2c.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/interrupt.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/lcd.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/mpu.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/onewire.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/pwm.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/qei.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/shamd5.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/ssi.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/sw_crc.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/sysctl.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/sysexc.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/systick.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/timer.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/uart.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/udma.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/usb.c \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/watchdog.c 

S_SRCS += \
C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/epi_workaround_ccs.s 

OBJS += \
./adc.obj \
./aes.obj \
./can.obj \
./comp.obj \
./cpu.obj \
./crc.obj \
./des.obj \
./eeprom.obj \
./emac.obj \
./epi.obj \
./epi_workaround_ccs.obj \
./flash.obj \
./fpu.obj \
./gpio.obj \
./hibernate.obj \
./i2c.obj \
./interrupt.obj \
./lcd.obj \
./mpu.obj \
./onewire.obj \
./pwm.obj \
./qei.obj \
./shamd5.obj \
./ssi.obj \
./sw_crc.obj \
./sysctl.obj \
./sysexc.obj \
./systick.obj \
./timer.obj \
./uart.obj \
./udma.obj \
./usb.obj \
./watchdog.obj 

S_DEPS += \
./epi_workaround_ccs.d 

C_DEPS += \
./adc.d \
./aes.d \
./can.d \
./comp.d \
./cpu.d \
./crc.d \
./des.d \
./eeprom.d \
./emac.d \
./epi.d \
./flash.d \
./fpu.d \
./gpio.d \
./hibernate.d \
./i2c.d \
./interrupt.d \
./lcd.d \
./mpu.d \
./onewire.d \
./pwm.d \
./qei.d \
./shamd5.d \
./ssi.d \
./sw_crc.d \
./sysctl.d \
./sysexc.d \
./systick.d \
./timer.d \
./uart.d \
./udma.d \
./usb.d \
./watchdog.d 

C_DEPS__QUOTED += \
"adc.d" \
"aes.d" \
"can.d" \
"comp.d" \
"cpu.d" \
"crc.d" \
"des.d" \
"eeprom.d" \
"emac.d" \
"epi.d" \
"flash.d" \
"fpu.d" \
"gpio.d" \
"hibernate.d" \
"i2c.d" \
"interrupt.d" \
"lcd.d" \
"mpu.d" \
"onewire.d" \
"pwm.d" \
"qei.d" \
"shamd5.d" \
"ssi.d" \
"sw_crc.d" \
"sysctl.d" \
"sysexc.d" \
"systick.d" \
"timer.d" \
"uart.d" \
"udma.d" \
"usb.d" \
"watchdog.d" 

S_DEPS__QUOTED += \
"epi_workaround_ccs.d" 

OBJS__QUOTED += \
"adc.obj" \
"aes.obj" \
"can.obj" \
"comp.obj" \
"cpu.obj" \
"crc.obj" \
"des.obj" \
"eeprom.obj" \
"emac.obj" \
"epi.obj" \
"epi_workaround_ccs.obj" \
"flash.obj" \
"fpu.obj" \
"gpio.obj" \
"hibernate.obj" \
"i2c.obj" \
"interrupt.obj" \
"lcd.obj" \
"mpu.obj" \
"onewire.obj" \
"pwm.obj" \
"qei.obj" \
"shamd5.obj" \
"ssi.obj" \
"sw_crc.obj" \
"sysctl.obj" \
"sysexc.obj" \
"systick.obj" \
"timer.obj" \
"uart.obj" \
"udma.obj" \
"usb.obj" \
"watchdog.obj" 

C_SRCS__QUOTED += \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/adc.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/aes.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/can.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/comp.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/cpu.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/crc.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/des.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/eeprom.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/emac.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/epi.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/flash.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/fpu.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/gpio.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/hibernate.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/i2c.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/interrupt.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/lcd.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/mpu.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/onewire.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/pwm.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/qei.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/shamd5.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/ssi.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/sw_crc.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/sysctl.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/sysexc.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/systick.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/timer.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/uart.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/udma.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/usb.c" \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/watchdog.c" 

S_SRCS__QUOTED += \
"C:/ti/TivaWare_C_Series-2.1.2.111/driverlib/epi_workaround_ccs.s" 


