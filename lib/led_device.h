/**
 * @file
 * @date		6/6/2016
 * @author 	Glenn Ellis
 *
 *
 */

#ifndef _LED_DEVICE_H
#define _LED_DEVICE_H

#include "pwm_device.h"

typedef struct LEDDevice
{
  PWMDevice pwm;
} LEDDevice;

void LED_Init(LEDDevice *led);
void LED_SetBrightness(LEDDevice *led, uint8_t value);
void LED_GetBrightness(LEDDevice *led);
void LED_TurnOn(LEDDevice *led);
void LED_TurnOff(LEDDevice *led);
void LED_Blink(LEDDevice *led);

#endif // _LED_DEVICE_H
