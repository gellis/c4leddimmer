/**
  @file
  @date   6/6/2016
  @author Glenn Ellis

*/
#include "common.h"
#include "rgb_led.h"

static void rgb_setup_pwm(RGBLed *rgb);
static uint8_t RGB_ValueToDutyCycle(uint8_t freq, uint8_t value);

void RGB_Init(RGBLed *rgb)
{
  rgb_setup_pwm(rgb);
  LED_Init(&rgb->red);
  LED_Init(&rgb->green);
  LED_Init(&rgb->blue);

  LED_TurnOn(&rgb->red);
  LED_TurnOn(&rgb->green);
  LED_TurnOn(&rgb->blue);
}

void RGB_SetBrightness(RGBLed *rgb, uint8_t value)
{
  rgb->brightness = value;
  LED_SetBrightness(&rgb->red, value);
  LED_SetBrightness(&rgb->green, value);
  LED_SetBrightness(&rgb->blue, value);
}

void RBG_RampUp(RGBLed *rgb, int ms)
{
  int i = rgb->brightness;
  while (i <= 100)
  {
    RGB_SetBrightness(rgb, i);
    i++;
    delay_ms(ms);
  }
}

void RBG_RampDown(RGBLed *rgb, int ms)
{
  int i = rgb->brightness;
  while (i >= 0)
  {
    RGB_SetBrightness(rgb, i);
    i--;
    delay_ms(ms);
  }
}

//-----------------------------------------------------------------------------
// Private functions
//-----------------------------------------------------------------------------
static uint8_t RGB_ValueToDutyCycle(uint8_t freq, uint8_t value)
{
  uint8_t percent = ((value * 100) / 255);
  uint32_t period = (SysCtlClockGet() / freq);
  return ((percent * period) / 100);
}

static void rgb_setup_pwm(RGBLed *rgb)
{
  PWMDevice *redPwm = &(rgb->red.pwm);
  PWMDevice *greenPwm = &(rgb->green.pwm);
  PWMDevice *bluePwm = &(rgb->blue.pwm);

  // Set up for PWM Modual 1
  redPwm->pwmPinCfg = GPIO_PF1_M1PWM5;
  greenPwm->pwmPinCfg = GPIO_PF2_M1PWM6;
  bluePwm->pwmPinCfg = GPIO_PF3_M1PWM7;

  redPwm->base = PWM1_BASE;
  greenPwm->base = PWM1_BASE;
  bluePwm->base = PWM1_BASE;

  redPwm->out = PWM_OUT_5;
  greenPwm->out = PWM_OUT_6;
  bluePwm->out = PWM_OUT_7;

  redPwm->bit = PWM_OUT_5_BIT;
  greenPwm->bit = PWM_OUT_6_BIT;
  bluePwm->bit = PWM_OUT_7_BIT;

  redPwm->gen = PWM_GEN_2;
  greenPwm->gen = PWM_GEN_3;
  bluePwm->gen = PWM_GEN_3;

  redPwm->periph = SYSCTL_PERIPH_PWM1;
  greenPwm->periph = SYSCTL_PERIPH_PWM1;
  bluePwm->periph = SYSCTL_PERIPH_PWM1;

  redPwm->modes = (PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
  greenPwm->modes = (PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
  bluePwm->modes = (PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);

  redPwm->gpioBase = GPIO_PORTF_BASE;
  greenPwm->gpioBase = GPIO_PORTF_BASE;
  bluePwm->gpioBase = GPIO_PORTF_BASE;

  redPwm->gpioPin = GPIO_PIN_1;
  greenPwm->gpioPin = GPIO_PIN_2;
  bluePwm->gpioPin = GPIO_PIN_3;
}
