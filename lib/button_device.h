/**
  @file

  @date Jun 6, 2016
  @author Glenn Ellis

*/

#ifndef BUTTON_DEVICE_H_
#define BUTTON_DEVICE_H_

#include <stdint.h>
#include <stdbool.h>

#include "iomap.h"
#include "common.h"
#include "driverlib/gpio.h"

typedef void (*ButtonFuncCB)();

typedef struct ButtonStruct
{
	uint32_t gpioPin;
	uint32_t holdTime;
	bool pressed;
} ButtonStruct;

typedef struct ButtonDevice
{
	ButtonFuncCB func;
	ButtonStruct button[BUTTON_NUM];
	uint8_t previousState;
} ButtonDevice;

void ButtonInit(ButtonDevice *btn);
uint32_t GetButtonHoldTime(ButtonStruct *btn);
bool IsButtonPressed(ButtonStruct *btn);
void ResetButtonTimer(ButtonStruct *btn);

#endif // BUTTON_DEVICE_H_
