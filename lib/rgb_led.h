/**
	@file

	@data 6/6/2016 3:49:24 PM
	
*/

#ifndef _RGB_LED_H
 #define _RGB_LED_H
#include "led_device.h"

typedef struct RGBLed
{
	LEDDevice red;
	LEDDevice green;
	LEDDevice blue;	
	uint8_t 	brightness; 	
}RGBLed;
 
 void RGB_Init(RGBLed *rgb); void RGB_SetColor(RGBLed *rgb);
 void RGB_SetBrightness(RGBLed *rgb,uint8_t value);
 void RGB_TurnOn(RGBLed *rgb);
 void RGB_TurnOff(RGBLed *rgb);
 void RBG_RampUp(RGBLed *rgb,int ms);
 void RBG_RampDown(RGBLed *rgb,int ms);
 void RGB_SetColor(RGBLed *rgb,uint8_t color);
 
#endif // _RGB_LED_H
