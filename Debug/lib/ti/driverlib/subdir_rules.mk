################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
lib/ti/driverlib/adc.obj: ../lib/ti/driverlib/adc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/adc.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/aes.obj: ../lib/ti/driverlib/aes.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/aes.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/can.obj: ../lib/ti/driverlib/can.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/can.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/comp.obj: ../lib/ti/driverlib/comp.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/comp.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/cpu.obj: ../lib/ti/driverlib/cpu.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/cpu.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/crc.obj: ../lib/ti/driverlib/crc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/crc.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/des.obj: ../lib/ti/driverlib/des.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/des.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/eeprom.obj: ../lib/ti/driverlib/eeprom.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/eeprom.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/emac.obj: ../lib/ti/driverlib/emac.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/emac.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/epi.obj: ../lib/ti/driverlib/epi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/epi.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/epi_workaround_ccs.obj: ../lib/ti/driverlib/epi_workaround_ccs.s $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/epi_workaround_ccs.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/flash.obj: ../lib/ti/driverlib/flash.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/flash.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/fpu.obj: ../lib/ti/driverlib/fpu.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/fpu.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/gpio.obj: ../lib/ti/driverlib/gpio.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/gpio.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/hibernate.obj: ../lib/ti/driverlib/hibernate.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/hibernate.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/i2c.obj: ../lib/ti/driverlib/i2c.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/i2c.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/interrupt.obj: ../lib/ti/driverlib/interrupt.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/interrupt.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/lcd.obj: ../lib/ti/driverlib/lcd.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/lcd.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/mpu.obj: ../lib/ti/driverlib/mpu.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/mpu.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/onewire.obj: ../lib/ti/driverlib/onewire.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/onewire.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/pwm.obj: ../lib/ti/driverlib/pwm.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/pwm.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/qei.obj: ../lib/ti/driverlib/qei.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/qei.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/shamd5.obj: ../lib/ti/driverlib/shamd5.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/shamd5.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/ssi.obj: ../lib/ti/driverlib/ssi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/ssi.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/sw_crc.obj: ../lib/ti/driverlib/sw_crc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/sw_crc.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/sysctl.obj: ../lib/ti/driverlib/sysctl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/sysctl.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/sysexc.obj: ../lib/ti/driverlib/sysexc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/sysexc.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/systick.obj: ../lib/ti/driverlib/systick.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/systick.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/timer.obj: ../lib/ti/driverlib/timer.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/timer.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/uart.obj: ../lib/ti/driverlib/uart.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/uart.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/udma.obj: ../lib/ti/driverlib/udma.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/udma.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/usb.obj: ../lib/ti/driverlib/usb.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/usb.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/ti/driverlib/watchdog.obj: ../lib/ti/driverlib/watchdog.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/ti/workspace/C4LedDimmer/lib/ti" --include_path="C:/ti/ccsv6/tools/compiler/arm_15.12.2.LTS/include" --include_path="C:/ti/workspace/C4LedDimmer" --include_path="C:/ti/workspace/C4LedDimmer/lib" -g --gcc --define=ccs="ccs" --define=PART_TM4C123GH6PM --diag_warning=225 --display_error_number --diag_wrap=off --abi=eabi --preproc_with_compile --preproc_dependency="lib/ti/driverlib/watchdog.d" --obj_directory="lib/ti/driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


