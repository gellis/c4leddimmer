################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lib/button_device.c \
../lib/common.c \
../lib/led_device.c \
../lib/pwm_device.c \
../lib/rgb_led.c 

OBJS += \
./lib/button_device.obj \
./lib/common.obj \
./lib/led_device.obj \
./lib/pwm_device.obj \
./lib/rgb_led.obj 

C_DEPS += \
./lib/button_device.d \
./lib/common.d \
./lib/led_device.d \
./lib/pwm_device.d \
./lib/rgb_led.d 

C_DEPS__QUOTED += \
"lib\button_device.d" \
"lib\common.d" \
"lib\led_device.d" \
"lib\pwm_device.d" \
"lib\rgb_led.d" 

OBJS__QUOTED += \
"lib\button_device.obj" \
"lib\common.obj" \
"lib\led_device.obj" \
"lib\pwm_device.obj" \
"lib\rgb_led.obj" 

C_SRCS__QUOTED += \
"../lib/button_device.c" \
"../lib/common.c" \
"../lib/led_device.c" \
"../lib/pwm_device.c" \
"../lib/rgb_led.c" 


