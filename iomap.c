/**
  @file
  @date   6/8/2016 2:37:25 PM
  @author Glenn Ellis
*/

#include "iomap.h"

void IOSetup()
{

  SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC |   SYSCTL_OSC_MAIN
                 | SYSCTL_XTAL_16MHZ);
  //Configure PWM Clock divide system clock by 64
  SysCtlPWMClockSet(SYSCTL_PWMDIV_64);

  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);

  //
  // Configure the GPIO Pin Mux for PF3
  // for M1PWM7
  //
  MAP_GPIOPinConfigure(GPIO_PF3_M1PWM7);
  MAP_GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_3);

  //
  // Configure the GPIO Pin Mux for PF2
  // for M1PWM6
  //
  MAP_GPIOPinConfigure(GPIO_PF2_M1PWM6);
  MAP_GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);

  //
  // Configure the GPIO Pin Mux for PF1
  // for M1PWM5
  //
  MAP_GPIOPinConfigure(GPIO_PF1_M1PWM5);
  MAP_GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_1);

  // Configure PWM Options
  PWMGenConfigure(PWM1_BASE,PWM_GEN_2,(PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC));
  PWMGenConfigure(PWM1_BASE,PWM_GEN_3,(PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC));

  //GPIOIntTypeSet(BUTTON_BASE,GPIO_FALLING_EDGE,ALL_BUTTONS);

}
